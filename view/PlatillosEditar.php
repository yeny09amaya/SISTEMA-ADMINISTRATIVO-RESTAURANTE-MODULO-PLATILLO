<?php

?>
<h1>
    <?php 

	if ($data->id != null){
        echo "Platillo con id: ". $data->id;
    }else{
        echo "Registrar nuevo plato de comida";
    } 
    ?>
</h1>


<form id="frm-Platillos" action="?controller=Platillos&accion=add" method="post">
    <input type="hidden" name="id" value="<?php echo $data->id; ?>" />
    
    <div class="form-group">
        <label>Categoría</label>
        <input type="text" name="categoria" value="<?php echo $data->categoria; ?>"  />
    </div>
    
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $data->nombre; ?>"  />
    </div>

    <div class="form-group">
        <label>Precio</label>
        <input type="text" name="precio" value="<?php echo $data->precio; ?>"  />
    </div>

    
    <hr />
    
    <div>
        <button>Guardar</button>
    </div>
</form>
