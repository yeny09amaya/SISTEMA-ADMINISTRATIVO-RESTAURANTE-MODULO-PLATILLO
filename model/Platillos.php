<?php

include_once("DB.php");

class Platillos{
    
    private $pdo;    
    public $id;
    public $categoria;
    public $nombre;
    public $precio;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT id, categoria, nombre, precio FROM platillo");

			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($id)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT id, categoria, nombre, precio FROM platillo WHERE id = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM platillo WHERE id = ?");			          

			$stm->execute(array($data->id));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE platillo SET 
						categoria = ?,
						nombre = ?,
						precio = ?
				    WHERE id = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array( 
        $data->categoria,
        $data->nombre,
        $data->precio,
        $data->id
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add(Platillos $data)
	{
		try{
		$sql = "INSERT INTO platillo(categoria,nombre,precio) 
		        VALUES (?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->categoria,
                    $data->nombre,
                    $data->precio
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}

?>