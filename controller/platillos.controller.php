<?php

require_once 'model/Platillos.php';

class PlatillosController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Platillos();
    }    
    public function Index(){
        require_once 'view/Platillos.php';
       
    }    
    public function Crud(){
        $data = new Platillos();
        
        if(isset($_REQUEST['id'])){
            $data = $this->model->getByID($_REQUEST['id']);
        }       
        require_once 'view/PlatillosEditar.php';          
    }  
    public function add(){
        $data = new Platillos();
        
        $data->id = $_REQUEST['id'];
        $data->categoria = $_REQUEST['categoria'];
        $data->nombre = $_REQUEST['nombre'];
        $data->precio = $_REQUEST['precio'];

        $data->id > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: IndexPlato.php');
    }
    
   
}

?>